#Reescribe_el_programa_del_cálculo_del_salario_para_darle_al_empleado
#1.5_veces_la_tarifa_horaria_para_todas_las_horas_trabajadas_que_excedan_de_40.
#Author_Wilmer_Cruz
#Email_Wilmer.cruz@unl.edu.ec

try:
    horas_trabajadas = float(input("Ingrese la cantidad de horas trabajadas: "))
    tarifa = float(input("Ingrese la tarifa: "))
    if horas_trabajadas > 40:
        horas_extras = (horas_trabajadas - 40)
        salario_horas_extras = (horas_extras * tarifa * 1.5) + (tarifa * 40)
        print ("El salario total del trabajador mas horas extras es de:", salario_horas_extras)
    if horas_trabajadas <= 40:
        salario = (horas_trabajadas * tarifa)
        print ("El salario del trabajador es de:", salario)
except:
        print ("Error, introdusca un número por favor")