#Reescribe el programa del salario usando try y except, de modo que el
#programa sea capaz de gestionar entradas no numéricas con elegancia, mostrando
#un mensaje y saliendo del programa.
#Autor: Wilmer Cruz
#Email: Wilmer.cruz@unl.edu.ec

try:
    horas_trabajadas = float(input("Ingrese la cantidad de horas trabajadas: "))
    tarifa = float(input("Ingrese la tarifa: "))
    horas_trabajadas = float(horas_trabajadas)
    tarifa = float(tarifa)
    salario = horas_trabajadas * tarifa
    print("El salario del trabajador es de:", salario)
except:
    print("Error, ingreso un carácter no valido, ingrese un número.")