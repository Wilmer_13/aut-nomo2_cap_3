#Escribe un programa que solicite una puntuación entre 0.0 y 1.0. Si la
#puntuación está fuera de ese rango, muestra un mensaje de error. Si la puntuación
#está entre 0.0 y 1.0, muestra la caliﬁcación usando la tabla siguiente:
#>= 0.9 Sobresaliente
#>= 0.8 Notable
#>= 0.7 Bien
#>= 0.6 Suficiente
#< 0.6 Insuficiente.
#Autor: Wilmer Cruz
#Email: Wilmer.cruz@unl.edu.ec
try:
    puntuación = float(input("introdusca la puntuacion"))

    if puntuación >= 0 and puntuación <= 1.0:
        if puntuación >= 0.9:
            print("Sobresaliente")
        elif puntuación >= 0.8:
            print("Notable")
        elif puntuación >= 0.7:
            print("Bien")
        elif puntuación >= 0.6:
            print("Suficiente")
        elif puntuación < 0.6:
            print("insuficiente")
    else:
        print("puntuacion incorrecta")

except:
    print("puntuacion incorrecta")